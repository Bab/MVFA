#define n  3
int number[n] = 0;
bool entering[n] = 0;
bool test = 0;
bool wrong = 0;

proctype P(int i) {
  entering[i] = 1;
  int k = 0;
  int max = 0;
  do
    :: (k < n) ->
	 if
	   :: (number[k] > max) -> max = number[k]
                   :: (number[k] <= max)  
         fi;
       k = k + 1
    :: (k >= n) -> break
  od;
  number[i] = max + 1;
  entering[i] = 0;     
  k = 0;
  do
    :: (k < n && !entering[k]) ->
	 (number[k] == 0 || number[k] > number[i] || (number[k] == number[i] && k >= i)) ->
         k = k + 1
    :: (k >= n) -> break
  od;

  if
  :: (test) -> wrong = 1
  :: (!test) -> test = 1; test = 0 
  fi;

  number[i] = 0;
}

proctype monitor() {
assert (wrong == 0)
}


init {
  int k =0;
  do
    :: (k < n) ->
	 run P(k);
         k = k + 1
    :: (k >= n) -> break
  od;
  run monitor();
}
