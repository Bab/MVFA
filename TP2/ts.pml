chan c[3]= [0] of {byte};

active proctype P1 () {
	byte v;
S1:	c[0]!2;
	printf("P1: transmit 2\n");
	goto S2;
S2: c[0]?v;
	printf("P1: receive %d\n", v);
	goto S1
}

active proctype P2 () {
	byte v;
S1: c[0]?v;
	printf("P2: receive %d\n", v);
	goto S2;
S2: c[0]!3;
	printf("P2: transmit 3\n");
	goto S1
}
