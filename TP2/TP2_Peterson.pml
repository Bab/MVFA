bool wait0 = 0;
bool wait1 = 1;
int tour;
bool test = 0;
bool wrong = 0;
proctype P0() {
  wait0 = 1;
  tour = 1;
  (!wait1 || tour == 0) ->
    if
    :: (test) -> wrong = 1
    :: (!test) -> test = 1; test = 0
    fi;
  wait0 = 0
}
proctype P1() {
  wait1= 1;
  tour = 0;
  (!wait0 || tour == 1) ->
    if
    :: (test) -> wrong = 1
    :: (!test) -> test = 1; test = 0
    fi;
  wait1 = 0;
}
init {
  run P0(); run P1();
  assert(!wrong)
}
