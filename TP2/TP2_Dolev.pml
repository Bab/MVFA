#define n  3
chan ch[n] = [3] of {int};
int nb_leader;

proctype P(int i) {
 int leader = i;
 int leader_prec = 0;
 int leader_prec_prec = 0;

int pred;
if
  :: (i == 0) -> pred = n - 1
  :: (i > 0) -> pred = i - 1
fi;

 ACT:
   ch[i]!leader;
   ch[pred]?leader_prec;
   if
    :: (leader == leader_prec) -> goto LEADER
    :: (leader != leader_prec) ->
      ch[i]!leader_prec;
      ch[pred]?leader_prec_prec;
      if
      :: (leader_prec > leader && leader_prec > leader_prec_prec) ->
          leader = leader_prec;
          goto ACT
      :: (!(leader_prec > leader && leader_prec > leader_prec_prec)) -> goto PASS
      fi
    fi
  PASS:
      ch[pred]?leader;
      ch[i]!leader;
      if
      :: (leader >= 0) -> goto PASS
      :: (leader < 0) -> goto END
      fi
  LEADER:
      nb_leader = nb_leader + 1;
      ch[i]!-1;
      goto END
  END:
}

proctype monitor() {
  assert (nb_leader <= 1)
}


init {
  int k =0;
  do
    :: (k < n) ->
	 run P(k);
         k = k + 1
    :: (k >= n) -> break
  od;
  run monitor();
}
