
MODULE main
VAR
  -- 0 represent the left stack, 1 the middle, and 2 the rightmost one.
  disk1 : 0..2;
  disk2 : 0..2;
  disk3 : 0..2;
  disk4 : 0..2;
  -- variable to decide which disk to move
  turn : 1..4;
ASSIGN
  init(turn) := 1;
  init(disk1) := 0;
  init(disk2) := 0;
  init(disk3) := 0;
  init(disk4) := 0;
  
  -- choose a disk to move
  next(turn) := {1, 2, 3, 4};
  
  next(disk1) :=
    case
      turn = 1 : -- it's our turn
        {(disk1 + 1) mod 3, (disk1 + 2) mod 3};
      TRUE : disk1; -- it's not
    esac;
  
  next(disk2) :=
    case
      turn = 2
      & disk1 != disk2 : -- disk1 isn't on top
        {(disk1 + 1) mod 3, (disk1 + 2) mod 3};
      TRUE : disk2;
    esac;
  
  next(disk3) :=
    case
      turn = 3
      & disk1 = disk2 -- disk 1 and 2 are in the same stack
      & disk1 != disk3 :  -- which is not the same as disk3
        {(disk1 + 1) mod 3, (disk1 + 2) mod 3};
      TRUE : disk3;
    esac;
      
  next(disk4) :=
    case
      turn = 4
      & disk1 = disk2 & disk2 = disk3
      & disk1 != disk4 :
        {(disk1 + 1) mod 3, (disk1 + 2) mod 3};
      TRUE : disk4;
    esac;

SPEC
AG !(disk1 = 2 & disk2 = 2 & disk3 = 2 & disk4 = 2)
