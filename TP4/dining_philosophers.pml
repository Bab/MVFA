#define n 4

bool forks[n];

int n_eat = 0;

proctype Philosopher(int tid) {
  int first = tid - 1;
  int second = tid;
  if
    :: (tid == 0) -> first = 0; second = n-1
  fi
  do
    :: atomic{forks[first] -> forks[first] = 0};
       atomic{forks[second] -> forks[second] = 0};
       n_eat = n_eat + 1;
       forks[first] = 0; forks[second] = 0;
       n_eat = n_eat - 1;
  od
  }

init {
  int k = 0;
  do
  :: (k < n) -> forks[k] = 1; k++
  :: (k >= n) -> break
  od;
  k = 0;
  do
    :: (k < n) -> run Philosopher(k); k++
    :: (k >= n) -> break
  od
}

ltl can_eat { [] <> (n_eat > 0)}
