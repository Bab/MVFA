int x = 0;
int finish = 0;
proctype P() {
  int y;
  int k;
  k = 0;
  do
  :: (k < 6) -> y = x;
                     y = x + 1;
                     x = y;
                     k = k+1
  :: (k >= 6) -> break
  od;
  finish = finish + 1
}
init {
  run  P(); run P(); run P();
  (finish == 3) -> assert (x != 2)
}
