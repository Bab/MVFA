bool wait0 = 0;
bool wait1 = 1;
int tour;
bool crit0 = 0;
bool crit1 = 0;
bool wrong = 0;
proctype P0() {
  wait0 = 1;
  tour = 1;
  (!wait1 || tour == 0) ->
    if
    :: (crit1) -> wrong = 1
    :: (!crit1) -> crit0= 1; crit0 = 0
    fi;
  wait0 = 0
}
proctype P1() {
  wait1= 1;
  tour = 0;
  (!wait0 || tour == 1) ->
    if
    :: (crit0) -> wrong = 1
    :: (!crit0) -> crit1 = 1; crit1 = 0
    fi;
  wait1 = 0;
}
init {
  run P0(); run P1();
  assert(!wrong)
}
ltl fairness {<> (crit0 == 1) && <> (crit1 == 1)}


